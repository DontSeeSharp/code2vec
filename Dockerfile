FROM tensorflow/tensorflow:2.0.0-gpu-py3

LABEL maintainer="Hendrig Sellik <h.sellik@student.tudelft.nl>" \
      description="Docker image to preprocess and train a code2vec model \
      for bug detectin."

COPY . /app

WORKDIR /app

RUN pip --no-cache-dir install -r /app/requirements-docker.txt

RUN apt-get update && apt-get install -y openjdk-8-jdk

RUN mkdir data

RUN chmod +x preprocess.sh && chmod +x train.sh

CMD ./preprocess.sh && ./train.sh